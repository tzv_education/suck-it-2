﻿using UnityEngine;

public class BlockerModel : MonoBehaviour
{
    public TierType blockerTier;
    public int XpForDestroy { get; set; }
}
